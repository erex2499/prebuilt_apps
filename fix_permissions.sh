#!/bin/bash
#Copyright (c) 2020 Divested Computing Group
#License: GPLv3

setStrict() {
	if [ -d "$1" ]; then
		find "$1" -type d -print0 | xargs -0 chmod -v 0700;
		find "$1" -type f -print0 | xargs -0 chmod -v 0600;
	fi;
}

setRelaxed() {
	if [ -d "$1" ]; then
		find "$1" -type d -print0 | xargs -0 chmod -v 0755;
		find "$1" -type f -print0 | xargs -0 chmod -v 0644;
	fi;
}

chmod -v 600 LICENSE* README.md;
chmod -v 700 *.sh;
setRelaxed android_vendor_FDroid_PrebuiltApps;
setRelaxed Fennec_DOS-Shim;
