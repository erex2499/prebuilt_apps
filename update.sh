#!/bin/bash
#DivestOS: A privacy focused mobile distribution
#Copyright (c) 2018-2020 Divested Computing Group
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

#export torW="torsocks"; #Uncomment to download over Tor

export outputPrefix="android_vendor_FDroid_PrebuiltApps/packages/";

export repoFD="https://f-droid.org/repo/";
export repoGP="https://guardianproject.info/fdroid/repo/";
export repoGPAlt="https://guardianproject.info/releases/";
export repoMG="https://microg.org/fdroid/repo/";

downloadAndVerifyApp() {
	umask 0022;
	$torW wget --directory-prefix=$outputPrefix "$1$3.apk";
	$torW wget --directory-prefix=$outputPrefix "$1$3.apk.asc";
	gpg2 --verify "$outputPrefix$3.apk.asc";
	mv -f "$outputPrefix$3.apk" "$outputPrefix$2.apk";
	mv -f "$outputPrefix$3.apk.asc" "$outputPrefix$2.apk.asc";
}
export -f downloadAndVerifyApp;

#TODO: Automate the updates
downloadAllApps() {
	##F-Droid
	downloadAndVerifyApp $repoFD F-DroidOfficial org.fdroid.fdroid_1016051;
	##Replacements
	downloadAndVerifyApp $repoFD EtarPrebuilt ws.xsoh.etar_34;
	downloadAndVerifyApp $repoFD OpenCamera net.sourceforge.opencamera_87;
	downloadAndVerifyApp $repoFD SimpleGallery com.simplemobiletools.gallery.pro_385;
	downloadAndVerifyApp $repoFD eSpeakNG com.reecedunn.espeak_20; #newer versions are missing dictionaries
	##Extras
	downloadAndVerifyApp $repoFD TalkBack com.android.talkback_370044210;
	downloadAndVerifyApp $repoFD TalkBackLegacy com.android.talkback_50007000;
}
export -f downloadAllApps;

importKeys() {
	#F-Droid - https://f-droid.org/en/docs/Release_Channels_and_Signing_Keys/
	gpg2 --recv-keys 37D2C98789D8311948394E3E41E7044E1DBA2E89;
	#Guardian Project - https://guardianproject.info/home/signing-keys/
	gpg2 --recv-keys 5E61C8780F86295CE17D86779F0FE587374BBE81; #hans
	gpg2 --recv-keys BBE20FD6DA48A3DD4CC7DF41A801183E69B37AA9; #nathan
	gpg2 --recv-keys 18930780A0433A61B8B217D697D05003DA731A17; #abel
	gpg2 --recv-keys 6F573CDC0E190E0F4C0AB1552A1E2A34308D1650; #halfparanoid
	gpg2 --recv-keys C85A83E6BE71EA3C8BA2FB163C0966BA81079F68; #semiparanoid
	#microG
	gpg2 --recv-keys 22F796D6E62E6625A0BCEFEA7F979A66F3E08422;
}
export -f importKeys;
